const baseUrl = "https://noroff-komputer-store-api.herokuapp.com/"
const bankBalanceElement = document.getElementById("bankBalance");
const loanParagraphElement = document.getElementById("loanParagraph");
const loanBalanceElement = document.getElementById("loan");
const loanBtnElement = document.getElementById("getLoanBtn");
const payLoanBtnElement = document.getElementById("payLoanBtn");
const payElement = document.getElementById("payBalance");
const bankBtnElement = document.getElementById("bankBtn");
const workBtnElement = document.getElementById("workBtn");
const computersElement = document.getElementById("computers");
const featuresElement = document.getElementById("features");
const pictureElement = document.getElementById("picture");
const pcTitleElement = document.getElementById("itemTitle");
const descriptionElement = document.getElementById("description");
const totalPriceElement = document.getElementById("totalPrice");
const buyBtnElement = document.getElementById("buyNowBtn");

let computers = [];
let bankBalance = 0;
let loanBalance = 0;
let salaryBalance = 0;
let totalPrice = 0;

// Fetching details about the computers
fetch(baseUrl+'computers')
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers));


const addComputersToList = (computers) => {
    computers.forEach(x => addComputerToList(x));
    pcTitleElement.innerText = computers[0].title;
    totalPrice = computers[0].price;
    totalPriceElement.innerText = totalPrice;
    descriptionElement.innerText = computers[0].description;
    pictureElement.src = baseUrl + computers[0].image;
    displayFeatures(computers[0].specs);
}

const addComputerToList = (computer) => {
    const computerElement = document.createElement('option');

    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}

const handleComputerDetailsChange = event => {
    const selectedComputer = computers[event.target.selectedIndex];
    totalPrice = selectedComputer.price;
    totalPriceElement.innerText = totalPrice;
    pcTitleElement.innerText = selectedComputer.title;
    descriptionElement.innerText = selectedComputer.description;
    pictureElement.src = baseUrl+selectedComputer.image;
    displayFeatures(selectedComputer.specs);
}

const displayFeatures = (features) => {
    featuresElement.innerText = '';
    features.forEach(x => displayFeature(x));
}

const displayFeature = (feature) => {
    const featureElement = document.createElement('li');
    featureElement.value = feature.id;
    featureElement.appendChild(document.createTextNode(feature));
    featuresElement.appendChild(featureElement);
}

const addSalaryToPayBalance = () => {
    salaryBalance += 100;
    payElement.innerText = salaryBalance;
}

const transferMoneyToBank = () => {
    // if there's a loan, 10% of the bank transfer should be "added"
    // to the loan balance.
    if(loanBalance > 0){
        const tenPercent = parseFloat(salaryBalance) * 0.1;
        salaryBalance -= tenPercent;
        bankBalance += salaryBalance;
        bankBalanceElement.innerText = bankBalance;

        loanBalance -= tenPercent;
        loanBalanceElement.innerText = loanBalance;

        // resets the value on the webpage and the salary variable
        payElement.innerText = 0;
        salaryBalance = 0;
    }
    if (loanBalance - salaryBalance === 0 || salaryBalance - loanBalance > 0){
        loanBtnElement.classList.remove("additionalBankBtnClass");
        payLoanBtnElement.style.display = "none";
        loanParagraphElement.style.display = "none";
    }
    bankBalance += salaryBalance;
    bankBalanceElement.innerText = bankBalance;
    payElement.innerText = 0;
    salaryBalance = 0;
}

// Checks if the user can take out a loan.
const handleLoan = () => {
    let desiredLoanAmount = prompt("Please enter the desired amount to loan: ");
    let intDesiredLoan = parseInt(desiredLoanAmount);

    if(isNaN(intDesiredLoan)){
        alert("Please only type numbers!");
    }
    else if(desiredLoanAmount > bankBalance * 2){
        alert("You cannot loan more money than double your bank balance! Please choose a smaller amount.");
    }
    else if(loanBalance > 0){
        alert("You already have a loan. You have to pay this back before taking up another loan!");
    }
    else if(intDesiredLoan > 0){
        loanBalance = intDesiredLoan;
        loanParagraphElement.style.display = "block";
        payLoanBtnElement.style.display = "unset";
        loanBtnElement.classList.add("additionalBankBtnClass");
        loanBalanceElement.innerText = loanBalance;
    }
}

const payDownLoan = () => {
    if (salaryBalance === 0){
        alert("Salary balance is empty.." +
            "You have to work some more to be able to pay down your loan.")
    }
    if (loanBalance > salaryBalance){
        loanBalance -= salaryBalance;
        loanBalanceElement.innerText = loanBalance;
        payElement.innerText = 0;
        salaryBalance = 0;
    }
    else if(salaryBalance > loanBalance){
        salaryBalance -= loanBalance;
        payElement.innerText = salaryBalance;
        loanBalanceElement.innerText = 0;
        loanBalance = 0;

        loanBtnElement.classList.remove("additionalBankBtnClass");
        payLoanBtnElement.style.display = "none";
        loanParagraphElement.style.display = "none";
    }
}

const buyComputer = () => {
    if( totalPrice > bankBalance + loanBalance) {
        alert("We're sorry.. You cannot afford this computer. Please come back later when you have enough money! :) ");
    }
    if(bankBalance > totalPrice || loanBalance > 0 && bankBalance + loanBalance >= totalPrice){
        if(bankBalance - totalPrice < 0){
            totalPrice -= bankBalance;
            bankBalance = 0;
            loanBalance -= totalPrice;

            bankBalanceElement.innerText = bankBalance;
            loanBalanceElement.innerText = loanBalance;
        }
        else{
            totalPrice -= bankBalance;
            bankBalance = 0;
            loanBalance -= totalPrice;
        }
        bankBalanceElement.innerText = bankBalance;
        loanBalanceElement.innerText = loanBalance;
    }

    bankBalanceElement.innerText = bankBalance;
    alert("Congratulations! You are the owner of a brand new computer!");
}

computersElement.addEventListener("change", handleComputerDetailsChange);
workBtnElement.addEventListener("click", addSalaryToPayBalance);
bankBtnElement.addEventListener("click", transferMoneyToBank);
loanBtnElement.addEventListener("click", handleLoan);
payLoanBtnElement.addEventListener("click", payDownLoan);
buyBtnElement.addEventListener("click", buyComputer);
// added an eventlistener to handle the broken image file
pictureElement.addEventListener("error", function (event){
    event.target.src = "assets/no-image-available.jpg";
    event.onerror = null;
})