# Assignment 1 - Dynamic Webpage in JS
I have tried to name my methods as explainable as possible, so there's not so many comments to describe what my methods do.

In addition to this, I chose to place all JS code in the same file due to the project's size.

## assets folder
I created an assets folder where I stored the image to replace the broken image to The Visor computer 

## CSS
I chose not to use Bootstrap as suggested in the assignment

### Link to my deployed website
https://hoelgc.gitlab.io/dynamicwebpagejs/
